#pragma once

#include "LogitechG27BlueprintLib.generated.h"

UCLASS()
class LOGITECHG27PLUGIN_API ULogitechG27BlueprintLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27", meta = (DisplayName = "Initialize G27"))
	static void SetProperties(bool useForceFeedback, int32 overallGain, int32 springGain, int32 damperGain, bool combinePedals, int32 wheelRange, bool enableDefaultSpring);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayLEDS(int32 minRpm, int32 maxRpm, int32 currentRpm);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayBumpyRoadEffect(int32 magnitude);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopBumpyRoadEffect();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayCarAirborne();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopCarAirborne();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayConstantForce(int32 magnitudePercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopConstantForce();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayDamperForce(int32 coefficientPercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopDamperForce();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayDirtRoadEffect(int32 magnitudePercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopDirtRoadEffect();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlayFrontalCollisionForce(int32 magnitudePercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlaySideCollisionForce(int32 magnitudePercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlaySlipperyRoadEffect(int32 magnitudePercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopSlipperyRoadEffect();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlaySoftstopForce(int32 usableRangePercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopSoftstopForce();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlaySpringForce(int32 offsetPercentage, int32 saturationPercentage, int32 coefficientPercentage);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopSpringForce();

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void PlaySurfaceEffect(int32 periodicType, int32 magnitude, int32 frequency);

	UFUNCTION(BlueprintCallable, Category = "Input|Logitech G27")
	static void StopSurfaceEffect();

};