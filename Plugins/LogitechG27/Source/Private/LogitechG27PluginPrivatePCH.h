// Copyright 1998 - 2015 Epic Games, Inc.All Rights Reserved.

#include "Core.h"
#include "CoreUObject.h"
#include "Engine.h"
#include "InputCoreClasses.h"
#include "IInputInterface.h"
#include "InputCoreTypes.h"
#include "ILogitechG27Plugin.h"

// You should place include statements to your module's private header files here.  You only need to
// add includes for headers that are used in most of your module's source files though.
#include "LogitechG27Lib.h"