// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "IInputDevice.h"
#include "LogitechG27Lib.h"

using namespace LogitechG27Lib;

class FLogitechG27Device : public IInputDevice
{
public:
	FLogitechG27Device(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler);
	virtual ~FLogitechG27Device();

	/** Tick the interface (e.g. check for new controllers) */
	virtual void Tick(float DeltaTime) override;

	/** Poll for controller state and send events if needed */
	virtual void SendControllerEvents() override;

	/** Set which MessageHandler will get the events from SendControllerEvents. */
	virtual void SetMessageHandler(const TSharedRef< FGenericApplicationMessageHandler >& InMessageHandler) override;

	/** Exec handler to allow console commands to be passed through for debugging */
	virtual bool Exec(UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar) override;

	/** IForceFeedbackSystem pass through functions **/
	virtual void SetChannelValue(int32 ControllerId, FForceFeedbackChannelType ChannelType, float Value) override;
	virtual void SetChannelValues(int32 ControllerId, const FForceFeedbackValues &values) override;

	bool IsDeviceAvailable();
	G27Lib _g27;
private:

	DeviceState _lastState; //last fetched state struct before updating
	int _selectedGear;

	void FLogitechG27Device::SendButtonUpEvent(FKey button);
	void FLogitechG27Device::SendButtonDownEvent(FKey button);
	void FLogitechG27Device::SendAxisEvent(FKey axis, float value);

	/* Message handler */
	TSharedRef<FGenericApplicationMessageHandler> MessageHandler;
	FKey _buttonKeys[23];
};