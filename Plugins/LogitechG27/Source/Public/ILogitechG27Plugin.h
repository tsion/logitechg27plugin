// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ModuleManager.h"
#include "IInputDeviceModule.h"
#include "LogitechG27Device.h"


/**
* The public interface to this module.  In most cases, this interface is only public to sibling modules
* within this plugin.
*/
class ILogitechG27Plugin : public IInputDeviceModule
{
public:

	/**
	* Singleton-like access to this module's interface.  This is just for convenience!
	* Beware of calling this during the shutdown phase, though.  Your module might have been unloaded already.
	*
	* @return Returns singleton instance, loading the module on demand if needed
	*/
	static inline ILogitechG27Plugin& Get()
	{
		return FModuleManager::LoadModuleChecked< ILogitechG27Plugin >("LogitechG27Plugin");
	}

	/**
	* Checks to see if this module is loaded and ready.  It is only valid to call Get() if IsAvailable() returns true.
	*
	* @return True if the module is loaded and ready to use
	*/
	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("LogitechG27Plugin");
	}

	virtual FLogitechG27Device GetDevice() = 0;

	static const FKey LogitechG27_Wheel;
	static const FKey LogitechG27_Accelerator;
	static const FKey LogitechG27_Brake;
	static const FKey LogitechG27_Clutch;
	static const FKey LogitechG27_POV;

	static const FKey LogitechG27_ShiftUpPaddle;
	static const FKey LogitechG27_ShiftDownPaddle;

	static const FKey LogitechG27_WheelButtonLeft1;
	static const FKey LogitechG27_WheelButtonLeft2;
	static const FKey LogitechG27_WheelButtonLeft3;

	static const FKey LogitechG27_WheelButtonRight1;
	static const FKey LogitechG27_WheelButtonRight2;
	static const FKey LogitechG27_WheelButtonRight3;

	static const FKey LogitechG27_ShifterButtonBlackTop;
	static const FKey LogitechG27_ShifterButtonBlackLeft;
	static const FKey LogitechG27_ShifterButtonBlackRight;
	static const FKey LogitechG27_ShifterButtonBlackBottom;

	static const FKey LogitechG27_ShifterButtonRed1;
	static const FKey LogitechG27_ShifterButtonRed2;
	static const FKey LogitechG27_ShifterButtonRed3;
	static const FKey LogitechG27_ShifterButtonRed4;

	static const FKey LogitechG27_ShifterGear1;
	static const FKey LogitechG27_ShifterGear2;
	static const FKey LogitechG27_ShifterGear3;
	static const FKey LogitechG27_ShifterGear4;
	static const FKey LogitechG27_ShifterGear5;
	static const FKey LogitechG27_ShifterGear6;
	static const FKey LogitechG27_ShifterGearReverse;
};


